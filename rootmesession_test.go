package rootmesession_test

import (
	"log"
	"net/http"

	"gitlab.com/worstone2/rootmesession"
)

func ExampleConnect() {
	url := "https://www.root-me.org/fr/Challenges/"

	// connect and create the sessionn for root-me.org.
	client, err := rootmesession.Connect()
	if err != nil {
		log.Fatal(err)
	}

	// Errors should be handled
	req, _ := http.NewRequest("GET", url, nil)
	res, _ := client.Do(req)
	log.Println("Response :", res.StatusCode)

	client.CloseIdleConnections()
}
