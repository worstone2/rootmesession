package rootmesession

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"time"
)

//
var cookieJar, _ = cookiejar.New(nil)

var transport = &http.Transport{
	MaxIdleConnsPerHost: 1024,
	TLSHandshakeTimeout: 0 * time.Second,
}

var client = &http.Client{
	Jar:       cookieJar,
	Transport: transport,
}

// askCredentials asks the user for his root-me.org credentials,
// nothing is persisted.
func askCredentials() (login string, password string) {
	fmt.Print("[root-me.org] login    : ")
	fmt.Scanln(&login)
	fmt.Print("[root-me.org] password : ")
	// psswd, _ := terminal.ReadPassword(0)
	fmt.Print("\033[8m") // hide input
	var passwd string
	fmt.Scanln(&passwd)
	fmt.Print("\033[28m") // show input
	// password = string(psswd)
	fmt.Println("")

	return login, password
}

// Connect is the root-me.org connection, it instanciates
// the needed cookies in order to persist the session.
func Connect() (*http.Client, error) {
	rmLogin, rmPassword := askCredentials()

	url := "https://www.root-me.org/?=&page=login&page=login&lang=fr&lang=fr&ajah=1&ajah=1&var_ajax=form&formulaire_action=login&formulaire_action_args=UazOXOeKJTVgiSfp0TpgGgfWOStUYRq6JMq1A2naUh0iRHLsEyNSce5V7rzdCUgFkl2kfMimmx9b9Boi62KxzXBy8BTs%2BsODjtMa9r4Cvhoalf1r0FuTEguE71boTTT3IYmQTqPDdFE%3D&var_login=" + rmLogin + "&password=" + rmPassword

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return nil, errors.New("You need to log in to root-me.org in a browser before doing this")
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		return nil, errors.New("You need to log in to root-me.org in a browser before doing this")
	}

	defer res.Body.Close()

	return client, nil
}
