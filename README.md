# RootMeSession

## Description
Simple way to make an http session in go for root-me.org.  
Just to avoid handling the cookies manually, it's replaced by a login prompt when starting the program.  
**:warning: It is required to login to the website first, then run the program.**

## Example
```go
package main

import (
    "log"
    rms "gitlab.com/worstone2/rootmesession"
)

func main() {
    // connect and create the sessionn for root-me.org.
    client, err := rms.Connect()
    if err != nil {
        log.Fatal(err)
    }

    // now do stuff with `client` just like a regular `*http.Client`
    // ...
}
```

## Note
This should be used only by those who understand what it is and how to do but just look for a shortcut.